﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlToolBox.MVVM.Model {
    class Path {
        public bool matchSearch { get; set; }
        public List<Link> Links { get; set; }

        public Path(bool matchSearch, List<Link> links) {
            this.matchSearch = matchSearch;
            Links = links;
        }

        public Path() {
            this.matchSearch = false;
            Links = new List<Link>();
        }

        public override string ToString() {
            StringBuilder response = new StringBuilder();
            foreach (Link link in this.Links) {
                response
                    .Append(link.tab1)
                    .Append("(" + link.tab1Key + ")")
                    .Append(" => ")
                    .Append("(" + link.tab2Key + ")");
            }
            response.Append(this.Links[this.Links.Count - 1].tab2);
            return response.ToString();
        }

        public string ToSql() {
            StringBuilder response = new StringBuilder();
            response.AppendFormat("Select * from {0} {5}{4}join {1} {6} on {5}.{3} = {6}.{2}{4}",
                Links[0].tab1, Links[0].tab2, Links[0].tab1Key, Links[0].tab2Key, Environment.NewLine, GetTrigrame(Links[0].tab1), GetTrigrame(Links[0].tab2));
            foreach (Link link in Links.GetRange(1,Links.Count - 1)) {
                response.AppendFormat("join {0} {5} on {6}.{3} = {5}.{2}{4}", link.tab2, link.tab1, link.tab2Key, link.tab1Key, Environment.NewLine, GetTrigrame(link.tab2), GetTrigrame(link.tab1));
            }
            return response.ToString();
        }

        private string GetTrigrame(string name) {
            StringBuilder sb = new StringBuilder();
            string[] names = name.Split('_');

            foreach(string n in names) {
                sb.Append(n.Substring(0, 1));
                if (n == names.Last() && sb.Length < 3) {
                        for(int i = 1; i < 3; i++) {
                        sb.Append(n.Substring(i, 1));
                        if (sb.Length == 3) break;
                    }
                } 
            }
            return sb.ToString().ToLower();
        }

        public string GetSqlByString (string s, List<Path> paths) {
             return paths.Where(p => p.ToString() == s).FirstOrDefault().ToSql();
        }
    }
}
