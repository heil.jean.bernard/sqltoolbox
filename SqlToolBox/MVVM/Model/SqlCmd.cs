﻿using SqlToolBox.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlToolBox.MVVM.Model {
    static class SqlCmd {
       public static string dataBases  = "select name from sys.databases";
       public static string tablesFromBase = "SELECT tab.name from sys.tables tab order by tab.name";
       public static string searchLink  = @"SELECT
                                tab1.name 'table1',
                                cp.name 'table1key',
	                            case
	                            when tab1.object_id<> tab2.object_id then tab2.name
                                when tab1.object_id = tab2.object_id then tab2bis.name
                                end
                                as 'table2',
                                cr.name 'table2key'

                            FROM sys.foreign_keys fk
                            JOIN sys.tables tab1 ON fk.parent_object_id = tab1.object_id OR fk.referenced_object_id = tab1.object_id
                            JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
                            JOIN sys.columns cp ON fkc.parent_column_id = cp.column_id AND fkc.parent_object_id = cp.object_id
                            join sys.tables tab2 on tab2.object_id = cp.object_id
                            join sys.columns cr ON fkc.referenced_column_id = cr.column_id AND fkc.referenced_object_id = cr.object_id
                            join sys.tables tab2bis on tab2bis.object_id = cr.object_id";
       public static string _activeDataBase = "master";
       public static string _activeTable;
       public static List<TableInfo> tableInfos = new List<TableInfo>();
       public static string analyseTable = "DECLARE @table AS VARCHAR(100)='"+_activeTable+ @"';SELECT table1key as 'Identifiant colonne',table2 'Table liée' from ( select
    fk.name 'FK Name',
    tab1.name 'table1',
    cp.name 'table1key',
	case
	when tab1.object_id <> tab2.object_id then tab2.name
	when tab1.object_id = tab2.object_id then tab2bis.name
	end
	as 'table2',
    cr.name 'table2key'

FROM 
    sys.foreign_keys fk
JOIN sys.tables tab1 ON fk.parent_object_id = tab1.object_id OR fk.referenced_object_id = tab1.object_id
JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
JOIN sys.columns cp ON fkc.parent_column_id = cp.column_id AND fkc.parent_object_id = cp.object_id
join sys.tables tab2 on tab2.object_id = cp.object_id
join sys.columns cr ON fkc.referenced_column_id = cr.column_id AND fkc.referenced_object_id = cr.object_id
join sys.tables tab2bis on tab2bis.object_id = cr.object_id

	where tab1.name = @table) linkedTable


	UNION 

	select * from
	(select
	
	col.name 'Identifiant colonne',
	' Pas de table liée' as 'Table liée'
	
 from sys.tables tab
join sys.columns col on col.object_id = tab.object_id
left join sys.foreign_key_columns fkc ON fkc.parent_object_id = tab.object_id and col.column_id = fkc.parent_column_id

where tab.name = @table AND fkc.constraint_column_id is null


) noLinkedTable";
        public static string activeDataBase {
            get { return _activeDataBase; }
            set {
                _activeDataBase= value;
                setListTable();
            }
        }
        public static string activeTable {
            get { return _activeTable; }
            set {
                _activeTable = value;
                setTableInfo();
            }
        }
        public static string ConnexionString (string dataBase) {
            return "server=ATEA;database=" + dataBase +";Integrated Security=sspi";
        }
        public static List<string> tableList { get; set; } = new List<string>();
        public static void setListTable() {
            tableList.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter(tablesFromBase, ConnexionString(_activeDataBase));
            DataSet tables = new DataSet();
            adapter.Fill(tables, "tables");
            foreach (DataRow row in tables.Tables[0].Rows) {
                tableList.Add(row.ItemArray[0].ToString());
            }
        }
        public static void setTableInfo() {
            tableInfos.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter(getAnalyseTable(), ConnexionString(_activeDataBase));
            DataSet tables = new DataSet();
            adapter.Fill(tables, "tables");
            foreach (DataRow row in tables.Tables[0].Rows) {
                tableInfos.Add(new TableInfo(row.ItemArray[0].ToString(), row.ItemArray[1].ToString()));
            }
        }

        private static string getAnalyseTable() {
            return "DECLARE @table AS VARCHAR(100)='" + _activeTable + @"';SELECT table1key as 'Identifiant colonne',table2 'Table liée' from ( select
    fk.name 'FK Name',
    tab1.name 'table1',
    cp.name 'table1key',
	case
	when tab1.object_id <> tab2.object_id then tab2.name
	when tab1.object_id = tab2.object_id then tab2bis.name
	end
	as 'table2',
    cr.name 'table2key'

FROM 
    sys.foreign_keys fk
JOIN sys.tables tab1 ON fk.parent_object_id = tab1.object_id OR fk.referenced_object_id = tab1.object_id
JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
JOIN sys.columns cp ON fkc.parent_column_id = cp.column_id AND fkc.parent_object_id = cp.object_id
join sys.tables tab2 on tab2.object_id = cp.object_id
join sys.columns cr ON fkc.referenced_column_id = cr.column_id AND fkc.referenced_object_id = cr.object_id
join sys.tables tab2bis on tab2bis.object_id = cr.object_id

	where tab1.name = @table) linkedTable


	UNION 

	select * from
	(select
	
	col.name 'Identifiant colonne',
	' No linked table' as 'Table liée'
	
 from sys.tables tab
join sys.columns col on col.object_id = tab.object_id
left join sys.foreign_key_columns fkc ON fkc.parent_object_id = tab.object_id and col.column_id = fkc.parent_column_id

where tab.name = @table AND fkc.constraint_column_id is null


) noLinkedTable";
        }
    }
}
