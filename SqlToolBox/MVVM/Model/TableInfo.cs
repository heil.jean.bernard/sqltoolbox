﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlToolBox.MVVM.Model {
    class TableInfo {

        public string colName { get; set; }
        public string linkedTable { get; set; }

        public TableInfo(string colName, string linkedTable) {
            this.colName = colName;
            this.linkedTable = linkedTable;
        }
    }
}
