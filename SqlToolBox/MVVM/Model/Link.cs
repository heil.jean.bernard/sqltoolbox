﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlToolBox.MVVM.Model {
    class Link {
        public Link(string tab1, string tab1Key, string tab2, string tab2Key) {
            this.tab1 = tab1;
            this.tab1Key = tab1Key;
            this.tab2 = tab2;
            this.tab2Key = tab2Key;
        }

        public string tab1 { get; set; }
        public string tab1Key { get; set; }
        public string tab2 { get; set; }
        public string tab2Key { get; set; }

        public override string ToString() {
            StringBuilder response = new StringBuilder();

            response
                .Append(this.tab1)
                .Append(" => ")
                .Append(this.tab2);

            return response.ToString();
        }
    }
}
