﻿using SqlToolBox.MVVM.Model;
using SqlToolBox.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SqlToolBox.MVVM.View {
    /// <summary>
    /// Interaction logic for PathFinderView.xaml
    /// </summary>
    public partial class PathFinderView : UserControl {

        private static List<Path> paths = new List<Path>();
        private static List<Link> linksRef = new List<Link>();
        public static string activeDataBase = string.Empty;
        const int d = 1000;

    public PathFinderView() {
            InitializeComponent();
            startTable.GotFocus += new RoutedEventHandler(ComboBox_DropDownOpened);
            endTable.GotFocus += new RoutedEventHandler(ComboBox_DropDownOpened);
            resultListBox.SelectionChanged += new SelectionChangedEventHandler(ShowSql);
            searchLinkButton.Click += new RoutedEventHandler(SearchLink);
            info.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(CopyToClipBoard);
        }

        private void CopyToClipBoard(object sender, EventArgs e) {
            Clipboard.SetText(info.Text);
            info.Text = "      Copied to the clipboard. ";
        }
        private void ShowSql(object sender, EventArgs e) {
            Path p = new Path();
            if (resultListBox.SelectedItem != null) info.Text = p.GetSqlByString(resultListBox.SelectedItem.ToString(), paths);
            PathFinderViewModel pathFinderVM = new PathFinderViewModel();

        }
        private void ComboBox_DropDownOpened(object sender, EventArgs e) {
            ((ComboBox)sender).Items.Clear();
            SqlCmd.tableList.ForEach(t => ((ComboBox)sender).Items.Add(t));
        }
        private async void SetInfoText(string s) {
            await Task.Delay(d);
            info.Text += s + Environment.NewLine;
            await Task.Delay(d);
        }
        private async Task SetLinkRef() {

            if (linksRef.Count == 0 || !activeDataBase.Equals(SqlCmd.activeDataBase)) {
                linksRef.Clear();
                activeDataBase = SqlCmd.activeDataBase;
                SetInfoText("Get data ..");
                await Task.Delay(d);
                DataSet linkTable = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter(SqlCmd.searchLink, SqlCmd.ConnexionString(SqlCmd.activeDataBase));
                adapter.Fill(linkTable, "linkTable");
                foreach (DataRow row in linkTable.Tables[0].Rows) {
                    Link l = new Link(row.ItemArray[0].ToString(), row.ItemArray[1].ToString(), row.ItemArray[2].ToString(), row.ItemArray[3].ToString());
                    linksRef.Add(l);
                }
            }
        }
        private async Task SetPath() {
            SetInfoText("Link searching ..");
            await Task.Delay(d);
            Path path = new Path();
            addLinkToPath(linksRef.FindAll(l => l.tab1 == startTable.Text), path);
            paths.Distinct().ToList()
                .FindAll(p => p.Links.FindAll(l => l.tab2 == endTable.Text).Count > 0)
                .OrderBy(op => op.Links.Count).ToList()
                .Select(os => os.ToString()).ToList().ForEach(p => resultListBox.Items.Add(p));
        }
        private async void SearchLink(object sender, EventArgs e) {
            info.Text = "";
            paths.Clear();
            resultListBox.Items.Clear();
            SetInfoText("Start...");
            await Task.Delay(d);
            await SetLinkRef();
            await Task.Delay(d);
           await SetPath();
           SetInfoText(resultListBox.Items.Count + " Link found.");
        }
        private void addLinkToPath(List<Link> links, Path path) {
            int _searchSize = Int32.Parse(searchSize.SelectedItem.ToString());
            double maxPath = Math.Pow((20 - _searchSize), _searchSize);
            double avancement = 0;
            foreach (Link link in links) {
                if (path.Links.FindAll(l => l.tab1 == link.tab2).Count < 1) path.Links.Add(link);
                else continue;
                path.matchSearch = link.tab2 == endTable.Text ? true : false;
                if (path.Links.Count < _searchSize && path.matchSearch == false) {
                    addLinkToPath(linksRef.FindAll(l => l.tab1 == link.tab2 && l.tab2 != link.tab1), path);
                } else if (path.Links.Count == _searchSize) {
                    paths.Add(new Path(path.matchSearch, path.Links.FindAll(l => l == l)));
                    path.Links.RemoveAt(path.Links.Count - 1);
                } else if (path.matchSearch) {
                    paths.Add(new Path(path.matchSearch, path.Links.FindAll(l => l == l)));
                    path.Links.RemoveAt(path.Links.Count - 1);
                    path.matchSearch = false;
                    continue;
                }
            }
            paths.Add(new Path(path.matchSearch, path.Links.FindAll(l => l == l)));
            if (path.Links.Count > 0) path.Links.RemoveAt(path.Links.Count - 1);

            if (avancement * 1000 < (paths.Count / maxPath)) {
                avancement = paths.Count / maxPath;
                //SetInfoText(avancement.ToString("P", CultureInfo.InvariantCulture));
            }
        }
    }
}
