﻿using SqlToolBox.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SqlToolBox.MVVM.View {
    /// <summary>
    /// Interaction logic for InputFormaterView.xaml
    /// </summary>
    public partial class InputFormaterView : UserControl {
        public InputFormaterView() {
            InitializeComponent();
            info.Text = "Copy data that you want to format and click on the button you need";
            whereInButton.Click += new RoutedEventHandler(WhereInFormat);
            insertIntoButton.Click += new RoutedEventHandler(InsertIntoFormat);
        }

        public void WhereInFormat(object sender, EventArgs e) {
          string input = Clipboard.GetText();
            StringBuilder result = new StringBuilder();
            if (!input.Contains("\t")) {
                result.Append("where in (").AppendLine();
                input.Split(new string[] { "\r\n" }, StringSplitOptions.None)
               .Where(x => !string.IsNullOrEmpty(x))
               .ToList()
               .ForEach(s => result.Append("'").Append(s).Append("',").AppendLine());
                result.Length-=3;
                result.Append(")");
                Clipboard.SetText(result.ToString());
                info.Foreground = Brushes.Green;
                info.Text = "Text format success";

            } else {
                info.Text = "Invalid input, need only one column for 'Where in' formating";
                info.Foreground = Brushes.Red;
            }
        }

        public void InsertIntoFormat(object sender, EventArgs e) {
            string input = Clipboard.GetText();
            StringBuilder result = new StringBuilder();
            if (input.Contains("\t")) {
                result.Append("INSERT INTO @Table").AppendLine().Append("(");
                List<string> rows = input.Split(new string[] { "\r\n" }, StringSplitOptions.None)
                .Where(x => !string.IsNullOrEmpty(x))
                .ToList();
                for (int i = 0; i < rows.Count; i++) {
                    if (i == 0) {
                        rows[i].Split(new string[] { "\t" }, StringSplitOptions.None)
                           .Where(x => !string.IsNullOrEmpty(x))
                           .ToList().ForEach(column => result.Append(column).Append(","));
                        result.Length--;
                        result.Append(")");
                    } else {
                        if (i == 1) result.AppendLine().Append("VALUES").AppendLine().Append("(");
                        else result.Append("(");

                        List<string> columns = rows[i].Split(new string[] { "\t" }, StringSplitOptions.None)
                        .Where(x => !string.IsNullOrEmpty(x))
                        .ToList();
                        rows[i].Split(new string[] { "\t" }, StringSplitOptions.None)
                        .Where(x => !string.IsNullOrEmpty(x))
                        .ToList().ForEach(s => result.Append("'").Append(s).Append("',"));
                        result.Length--;
                        result.Append("),").AppendLine();
                    }
                }
                result.Length -= 3;
                Clipboard.SetText(result.ToString());
                info.Foreground = Brushes.Green;
                info.Text = "Text format success";

            } else {
                info.Text = "Invalid input, need many column for 'Insert into .. Values ..' formating";
                info.Foreground = Brushes.Red;
            }
        }
    }
}
