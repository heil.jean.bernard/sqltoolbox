﻿using SqlToolBox.MVVM.Model;
using SqlToolBox.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SqlToolBox.MVVM.View {
    /// <summary>
    /// Interaction logic for TableAnalyseView.xaml
    /// </summary>
    public partial class TableAnalyseView : UserControl {

        public TableAnalyseView() {
            InitializeComponent();
            focusTable.GotFocus += new RoutedEventHandler(ComboBox_DropDownOpened);
            searchTableInfoButton.Click += new RoutedEventHandler(SearchInfo);
            tableInfoGrid.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(SetActiveTable);
           
    }
        private void ComboBox_DropDownOpened(object sender, EventArgs e) {
            ((ComboBox)sender).Items.Clear();
            SqlCmd.tableList.ForEach(t => ((ComboBox)sender).Items.Add(t));
        }

        private void SearchInfo (object sender, EventArgs e) {
            SqlCmd.activeTable = focusTable.Text;
            tableInfoGrid.ItemsSource = SqlCmd.tableInfos;
        }

        private void SetActiveTable(object sender, EventArgs e) {

            if(tableInfoGrid.SelectedItem != null && !((TableInfo)tableInfoGrid.SelectedItem).linkedTable.Equals(" No linked table")) {


                SqlCmd.activeTable = ((TableInfo)tableInfoGrid.SelectedItem).linkedTable;
                focusTable.Text = ((TableInfo)tableInfoGrid.SelectedItem).linkedTable;
                tableInfoGrid.ItemsSource = SqlCmd.tableInfos;
                tableInfoGrid.Items.Refresh();
               
            }

        }
    }
}
