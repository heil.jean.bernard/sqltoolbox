﻿using SqlToolBox.Core;
using SqlToolBox.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlToolBox.MVVM.ViewModel {
    class MainViewModel : ObservableObject {

        public RelayCommand HomeViewCommand { get; set; }
        public RelayCommand PathFinderViewCommand { get; set; }
        public RelayCommand TableAnalyseViewCommand { get; set; }
        public RelayCommand InputFormaterViewCommand { get; set; }
        public List<string> dataBaseList { get; set; } = new List<string>();

        private string _activeDatabase;
        public string activeDatabase {
            get { return _activeDatabase; }
            set {
                _activeDatabase = value;
                SqlCmd.activeDataBase = _activeDatabase;
                OnPropertyChanged();
            }
        }

        public HomeViewModel HomeVM;
        public PathFinderViewModel PathFinderVM;
        public TableAnalyseViewModel TableAnalyseVM;
        public InputFormaterViewModel InputFormaterVM;
        private object _currentView;

        public object CurrentView {
            get { return _currentView; }
            set {
                _currentView = value;
                OnPropertyChanged();
            }
        }

        public void setListDatabase() {
            SqlDataAdapter adapter = new SqlDataAdapter(SqlCmd.dataBases, SqlCmd.ConnexionString("master"));
            DataSet tables = new DataSet();
            adapter.Fill(tables, "tables");
            foreach (DataRow row in tables.Tables[0].Rows) {
                dataBaseList.Add(row.ItemArray[0].ToString());
            }
        }


        public MainViewModel() {
            setListDatabase();

            HomeVM = new HomeViewModel();
            PathFinderVM = new PathFinderViewModel();
            TableAnalyseVM = new TableAnalyseViewModel();
            InputFormaterVM = new InputFormaterViewModel();
            CurrentView = HomeVM;

            HomeViewCommand = new RelayCommand(o => { CurrentView = HomeVM; });
            PathFinderViewCommand = new RelayCommand(o => { CurrentView = PathFinderVM; });
            TableAnalyseViewCommand = new RelayCommand(o => { CurrentView = TableAnalyseVM; });
            InputFormaterViewCommand = new RelayCommand(o => { CurrentView = InputFormaterVM; });

        }






    }
}
