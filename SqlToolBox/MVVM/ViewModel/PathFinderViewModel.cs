﻿using SqlToolBox.Core;
using SqlToolBox.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace SqlToolBox.MVVM.ViewModel {
    class PathFinderViewModel : ObservableObject {

        private string _endTable;
        public string endTable {
            get { return _endTable; }
            set {
                _endTable = value;
                OnPropertyChanged();
            }
        }

        private string _startTable;
        public string startTable {
            get { return _startTable; }
            set {
                _startTable = value;
                OnPropertyChanged();
            }
        }

        private int _searchSize;
        public int searchSize {
            get { return _searchSize; }
            set {
                _searchSize = value;
                OnPropertyChanged();
            }
        }

        public List<int> joinList { get; set; } = Enumerable.Range(1, 5).ToList();

 
        public PathFinderViewModel() {
          
        }
    }
}
